IMAGE_NAME=gitlab.com/mironet/websocket-test
DOCKER_FILE=docker/Dockerfile
TARGET=websocket-test

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


all: clean build build-docker ## Clean and rebuild.

build: ## build the test program
	go build -o $(TARGET) -ldflags "-X main.version=$$(git describe --always)"

build-docker: export DOCKER_BUILDKIT = 1
build-docker: ## Build the Docker image.
	docker build --build-arg APP_VERSION=$$(git describe --always) -t $(IMAGE_NAME) -f $(DOCKER_FILE) .

clean: down ## Clean up.
	rm -vf $(TARGET)

clean-image: down ## Clean docker image too.
	docker image rm $(IMAGE_NAME) || exit 0

list-deps: ## list all imported packages
	go list -f '{{ join .Imports "\n" }}' ./...

test: ## run all tests.
	go vet $$(go list ./... | grep -v /vendor/)
	go test -race $$(go list ./... | grep -v /vendor/) -count 1